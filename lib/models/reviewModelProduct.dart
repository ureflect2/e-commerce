// ignore_for_file: file_names

class RatingReviewmodel {
  final String name, comment, img, date;
  final double rating;

  RatingReviewmodel(
      {required this.date,
      required this.rating,
      required this.name,
      required this.comment,
      required this.img});
}

List<RatingReviewmodel> reviews = [
  RatingReviewmodel(
      comment:
          'El montag mia el mia bs yaa reet ybcaa  fy aloan tania .Well done!',
      img: "assets/Screenshot_2023-11-03_235241-removebg-preview.png",
      name: 'Abo Ayman',
      rating: 4,
      date: '04 Nov, 2023'),
  RatingReviewmodel(
      comment:
          'El montag mia el mia bs yaa reet ybcaa  fy aloan tania .Well done!',
      img: "assets/OIP-removebg-preview.png",
      name: 'كروان مشاكل',
      rating: 3,
      date: '01 Nov, 2023'),
  RatingReviewmodel(
      comment:
          'El montag mia el mia bs yaa reet ybcaa  fy aloan tania .Well done!',
      img:
          "assets/293191926_3194015350833391_919019878836629539_n-removebg-preview.png",
      name: 'Me',
      rating: 5,
      date: '31 Oct, 2023'),
  RatingReviewmodel(
      comment:
          'El montag mia el mia bs yaa reet ybcaa  fy aloan tania .Well done!',
      img: "assets/Screenshot_2023-11-03_235241-removebg-preview.png",
      name: 'Abo Ayman',
      rating: 4,
      date: '04 Nov, 2023'),
  RatingReviewmodel(
      comment:
          'El montag mia el mia bs yaa reet ybcaa  fy aloan tania .Well done!',
      img: "assets/OIP-removebg-preview.png",
      name: 'كروان مشاكل',
      rating: 3,
      date: '01 Nov, 2023'),
  RatingReviewmodel(
      comment:
          'El montag mia el mia bs yaa reet ybcaa  fy aloan tania .Well done!',
      img:
          "assets/293191926_3194015350833391_919019878836629539_n-removebg-preview.png",
      name: 'Me',
      rating: 5,
      date: '31 Oct, 2023'),
  RatingReviewmodel(
      comment:
          'El montag mia el mia bs yaa reet ybcaa  fy aloan tania .Well done!',
      img: "assets/Screenshot_2023-11-03_235241-removebg-preview.png",
      name: 'Abo Ayman',
      rating: 4,
      date: '04 Nov, 2023'),
  RatingReviewmodel(
      comment:
          'El montag mia el mia bs yaa reet ybcaa  fy aloan tania .Well done!',
      img: "assets/OIP-removebg-preview.png",
      name: 'كروان مشاكل',
      rating: 3,
      date: '01 Nov, 2023'),
  RatingReviewmodel(
      comment:
          'El montag mia el mia bs yaa reet ybcaa  fy aloan tania .Well done!',
      img:
          "assets/293191926_3194015350833391_919019878836629539_n-removebg-preview.png",
      name: 'Me',
      rating: 5,
      date: '31 Oct, 2023'),
];
